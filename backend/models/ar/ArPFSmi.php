<?php
namespace backend\models\ar;


use yii\db\ActiveRecord;

class ArPFSmi extends ActiveRecord
{
    public static function tableName()
    {
        return 'PFSmi';
    }

    public static function getById($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->one();
    }

    public static function removeFromCatalog($id)
    {
        $ar_pfsmi = static::getById($id);
        $ar_pfsmi->is_catalog = 0;
        return $ar_pfsmi->save();
    }

    public static function addToCatalog($id)
    {
        $ar_pfsmi = static::getById($id);
        $ar_pfsmi->is_catalog = 1;
        return $ar_pfsmi->save();
    }

    public static function close($id)
    {
        $ar_pfsmi = static::getById($id);
        $ar_pfsmi->is_closed = 1;
        return $ar_pfsmi->save();
    }

    public static function open($id)
    {
        $ar_pfsmi = static::getById($id);
        $ar_pfsmi->is_closed = 0;
        return $ar_pfsmi->save();
    }

    public static function approved($id, $status)
    {
        $ar_pfsmi = static::getById($id);
        $ar_pfsmi->is_approved = $status;
        return $ar_pfsmi->save();
    }
}