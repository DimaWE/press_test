<?php
namespace backend\models\smi;


use backend\models\ar\ArGeo;
use backend\models\ar\ArPFSmi;
use yii\base\Model;

class SmiEdit extends Model
{

    public $id;
    public $name;
    public $site;
    public $type;
    public $industry;
    public $city;
    public $geo_id;
    public $is_top;
    public $is_approved;

    public function rules()
    {
        return [
            [[
                'id',
                'name',
                'site',
                'type',
                'industry',
                'city',
            ], 'required'],
            [[
                'name',
                'site',
                'type',
                'industry',
                'city',
            ], 'string', 'min' => 1, 'max' => 250, 'tooLong' => 'Заголовок не должен быть более 250 символов. '],
            [[
                'id',
                'geo_id',
                'is_top',
                'is_approved'
            ], 'integer'],
        ];
    }

    public function loadData()
    {
        $ar_smi = ArPFSmi::getById($this->id);
        $this->id = $ar_smi->id;
        $this->name = $ar_smi->name;
        $this->site = $ar_smi->site;
        $this->type = $ar_smi->type;
        $this->industry = $ar_smi->industry;
        $this->geo_id = $ar_smi->geo_id;
        $this->is_top = $ar_smi->is_top;
        $this->is_approved = $ar_smi->is_approved;

        $ar_geo = ArGeo::getById($ar_smi->geo_id);

        $this->city = $ar_geo->name;

    }

    public function save()
    {
        $ar_smi = ArPFSmi::getById($this->id);
        $ar_smi->name = $this->name;
        $ar_smi->site = $this->site;
        $ar_smi->type = $this->type;
        $ar_smi->industry = $this->industry;
        $ar_smi->geo_id = $this->geo_id;
        $ar_smi->is_approved = $this->is_approved;
        $ar_smi->is_top = $this->is_top;

        return $ar_smi->save();
    }
}