<?php
namespace backend\models\smi;


class ApproveList
{

    private static $list;

    public static function get()
    {
        if (static::$list == null) {
            $list = new ApproveList();
            static::$list = $list->getList();
        }

        return static::$list;
    }

    private function getList()
    {
        return  [
            '0' => 'Подана',
            '1' => 'Одобрена',
            '2' => 'Отклонена',
        ];
    }
}