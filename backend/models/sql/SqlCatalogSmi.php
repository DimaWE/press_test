<?php
namespace backend\models\sql;


use backend\models\smi\IndustryList;

class SqlCatalogSmi extends SqlAbstract
{

    const FIELDS_DEFAULT  = [
        'pf.id',
        'pf.name',
        'pf.site',
        'pf.type',
        'pf.period',
        'pf.industry',
        'pf.geo_id',
        'g.name as city',
        'SUM(IF(us.verification = 2, 1, 0)) as count_verification_journalists',
        'pf.is_catalog',
        'pf.is_closed',
        'pf.is_approved',
        'pf.updated_at'
    ];

    private $smi;
    private $city;
    private $site;
    private $industry;
    private $type;

    /**
     * @param mixed $smi
     * @return $this
     */
    public function setSmi($smi)
    {
        $this->smi = $smi;
        return $this;
    }

    /**
     * @param mixed $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @param mixed $site
     * @return $this
     */
    public function setSite($site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @param mixed $industry
     * @return $this
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
        return $this;
    }

    /**
     * @param mixed $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function build()
    {
        $this->command
            ->select($this->getFields())
            ->from('PFSmi as pf')
            ->leftJoin('UserSmi as us', 'pf.id = us.pfsmi_id')
            ->innerJoin('Geo as g', 'pf.geo_id = g.id')
            ->where([
                //'pf.is_approved' => 1
            ])
            ->groupBy('pf.id')
            ->orderBy('pf.id DESC');

        $this->createWhere();

        return $this;
    }

    private function createWhere()
    {
        //print_r($this->city);
        //exit;

        if ($this->smi) {
            $this->command
                ->andWhere(['like', 'pf.name', $this->smi]);
        }
        if ($this->city) {
            $this->command
                ->andWhere(['like', 'g.name', $this->city]);
        }
        if ($this->site) {
            $this->command
                ->andWhere(['like', 'pf.site', $this->site]);
        }
        if ($this->industry) {
            $this->industry = $this->industry == IndustryList::NOT_INDUSTRY ? '' : $this->industry;
            $this->command
                ->andWhere(['pf.industry' => $this->industry]);
        }
        if ($this->type) {
            $this->command
                ->andWhere(['pf.type' => $this->type]);
        }
    }
}