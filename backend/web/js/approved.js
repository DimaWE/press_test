$(".approved").on("change", function() {
    $.ajax({
        url: "/catalog-smi/approved",
        method: "GET",
        data: {id: $(this).data("smi-id") ,status: $(this).val()},
        success: function(data) {
            if(data) {
                data = JSON.parse(data);
                $("#myflashwrapper").html(data.message).fadeIn().delay(3000).fadeOut();
            }
        }
    });
});