<?php
/* @var $ar_smi \backend\models\ar\ArPFSmi */
/* @var $model \backend\models\smi\SmiEdit */
/* @var $this yii\web\View */
$this->title = 'Редактирование СМИ';


use backend\models\smi\IndustryList;
use backend\models\smi\TypeList;
use backend\models\smi\ApproveList;
use backend\models\ar\ArGeo;
use yii\bootstrap\Html;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css',['depends' => 'yii\bootstrap\BootstrapAsset']);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('js/smiedit.js', ['depends' => 'yii\web\YiiAsset']);

?>

<div class="row" style="margin-top: 40px">
    <div class="col-md-6">

        <?php
        $form = ActiveForm::begin([
            'action' => ['smi/save'],
            'id' => 'edit-smi-form',
            'options' => ['class' => ''],
            'method' => 'post',
        ]) ?>

        <?= $form
            ->field($model, 'geo_id')
            ->hiddenInput(['value'=> $model->geo_id])
            ->label(false)
        ?>

        <?= $form
            ->field($model, 'id')
            ->hiddenInput(['value'=> $model->id])
            ->label(false)
        ?>

        <?= $form
            ->field($model, 'name')
            ->label('Название')
        ?>
        <?= $form
            ->field($model, 'site')
            ->label('Сайт')
        ?>

        <?= $form->field($model, 'city')->dropDownList($cities, [
                'class' => 'selectpicker',
                'data-live-search' => 'true',
            ])
            ->label('Город') ?>

        <?= $form->field($model, 'type')->dropDownList(
            TypeList::get()
        )->label('Тип') ?>

        <?= $form->field($model, 'industry')->dropDownList(
            IndustryList::get()
        )->label('Рубрика') ?>

        <?= $form->field($model, 'is_approved')->dropDownList(
            ApproveList::get()
        )->label('Статус заявки') ?>

        <?= $form->field($model, 'is_top')->checkbox(['label' => ''])->label('Топ СМИ') ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            <a href="<?=Url::to(['catalog-smi/list'])?>"><?= Html::button('Отмена', ['class' => 'btn btn-default']) ?></a>
        </div>

        <?php ActiveForm::end() ?>

    </div>
</div>