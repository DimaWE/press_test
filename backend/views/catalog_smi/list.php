<?php
/* @var $this yii\web\View */
$this->title = 'Каталог СМИ';


use backend\components\StatusWidget;
use backend\helpers\Http;
use backend\models\smi\IndustryList;
use backend\models\smi\StatusCatalog;
use backend\models\smi\StatusClosed;
use backend\models\smi\TypeList;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\smi\ApproveList;

$this->registerJsFile('js/approved.js', ['depends' => 'yii\web\YiiAsset']);

?>

<style>
    td {
        word-wrap: break-word; /* Перенос слов */
    }
</style>
<div id="myflashwrapper" style="display: none;" class="alert alert-success"></div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:35%'],
            'attribute' => 'smi',
            'label' => 'СМИ',
            'format' => 'raw',
            'value' => function ($data) {

                return
                    '<br>' .
                    '<small>' .
                        '<a href="https://pressfeed.ru/smi/'. $data['id'] .'" target="_blank">' .
                            $data['name'] .
                        '</a>' .
                    '</small>' .
                    '<br>' .
                    '';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:35%'],
            'attribute' => 'city',
            'label' => 'Город',
            'format' => 'raw',
            'value' => function ($data) {

                return
                    '<small>' .
                        $data['city'] .
                    '</small>' .
                    '';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'site',
            'label' => 'Сайт',
            'format' => 'raw',
            'value' => function ($data) {

                return
                    '<small>' .
                        '<a href="'. Http::add($data['site']) .'" target="_blank">' .
                            $data['site'] .
                        '</a>' .
                    '</small>';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'name',
            'label' => 'Количество верифицированных журналистов',
            'format' => 'raw',
            'value' => function ($data) {

                return
                    '<small>' .
                        $data['count_verification_journalists'] .
                    '</small>';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'industry',
            'label' => 'Рубрика',
            'filter' => ArrayHelper::merge([IndustryList::NOT_INDUSTRY => '== Нет рубрики =='], IndustryList::get()),
            'format' => 'raw',
            'value' => function ($data) {

                //print_r(ArrayHelper::merge([IndustryList::NOT_INDUSTRY => 'Нет рубрики'], IndustryList::get()));

                return
                    '<small>' .
                        $data['industry'] .
                    '</small>';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'type',
            'label' => 'Тип',
            'filter' => TypeList::get(),
            'format' => 'raw',
            'value' => function ($data) {

                //print_r(ArrayHelper::merge([IndustryList::NOT_INDUSTRY => 'Нет рубрики'], IndustryList::get()));

                return
                    '<small>' .
                        $data['type'] .
                    '</small>';
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'is_approved',
            'label' => 'Статус заявки',
            'format' => 'raw',
            'value' => function ($data) {
                return Html::dropDownList('list', $data['is_approved'], ApproveList::get(), ['class' => 'approved', 'data-smi-id' => $data['id']]);
            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'buttons',
            'label' => 'Показывать на сайте',
            'format' => 'raw',
            'value' => function ($data) {

                return getCloseButtonHtml($data) .
                        getInCatalogButtonHtml($data) .
                        getEditButtonHtml($data);

            },
        ],
        [
            'class' => 'yii\grid\DataColumn',
            'headerOptions' => ['style' => 'width:5%'],
            'attribute' => 'updated_at',
            'label' => 'Дата изменения',
            'format' => 'raw',
            'value' => function ($data) {

                return
                    '<small>' .
                        $data['updated_at'] .
                    '</small>';

            },
        ],
    ],
]);


function getCloseButtonHtml($data)
{
    $types = [
        StatusClosed::OPENED => StatusWidget::SUCCESS,
        StatusClosed::CLOSED => StatusWidget::DANGER,
    ];

    if ($data['is_closed']) {

        $html =
            Html::a(StatusWidget::widget([
                'type' => $types[$data['is_closed']],
                'text' => StatusClosed::RU_NAME[$data['is_closed']]
            ]), ['open', 'id' => $data['id']], [
                'title' => 'Открыть СМИ',
                'data' => [
                    //'confirm' => 'Уверены, что хотите открыть СМИ',
                    'method' => 'post',
                ],
            ]);
    } else {

        $html =
            Html::a(StatusWidget::widget([
                'type' => $types[$data['is_closed']],
                'text' => StatusClosed::RU_NAME[$data['is_closed']]
            ]), ['close', 'id' => $data['id']], [
                'title' => 'Закрыть СМИ',
                'data' => [
                    //'confirm' => 'Уверены, что хотите закрыть СМИ?',
                    'method' => 'post',
                ],
            ]);
    }

    $html .= '<br>';

    return $html;
}

function getInCatalogButtonHtml($data)
{
    $types = [
        StatusCatalog::IN_CATALOG => StatusWidget::SUCCESS,
        StatusCatalog::NOT_IN_CATALOG => StatusWidget::DANGER,
    ];

    if ($data['is_catalog']) {

        $html =
            Html::a(StatusWidget::widget([
                'type' => $types[$data['is_catalog']],
                'text' => StatusCatalog::RU_NAME[$data['is_catalog']]
            ]), ['remove', 'id' => $data['id']], [
                'title' => 'Удалить СМИ из каталога',
                'data' => [
                    //'confirm' => 'Уверены, что хотите удалить СМИ из каталога',
                    'method' => 'post',
                ],
            ]);
    } else {

        $html =
            Html::a(StatusWidget::widget([
                'type' => $types[$data['is_catalog']],
                'text' => StatusCatalog::RU_NAME[$data['is_catalog']]
            ]), ['add', 'id' => $data['id']], [
                'title' => 'Зазместить СМИ в каталоге',
                'data' => [
                    //'confirm' => 'Уверены, что хотите размесить СМИ в каталоге?',
                    'method' => 'post',
                ],
            ]);
    }

    return $html;
}

function getEditButtonHtml($data)
{
    return
        '<br><br>' .
        Html::a('Редактировать', ['/smi/edit', 'id' => $data['id']], [
            'title' => 'Редактировать СМИ',
            'data' => [
                //'confirm' => 'Уверены, что хотите удалить СМИ из каталога',
                'method' => 'post',
            ],
        ]);
}

?>