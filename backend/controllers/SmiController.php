<?php
namespace backend\controllers;


use backend\models\smi\SmiEdit;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use backend\models\ar\ArGeo;

class SmiController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['be_smi_edit'],
                    ],
                ],
            ]
        ]);
    }

    public function actionEdit()
    {
        $request = Yii::$app->request;

        $model = new SmiEdit();
        $model->id = (int)$request->get('id');
        $model->loadData();
 
        $cities = ArGeo::find()->select('id, name')->all();
        $cities = ArrayHelper::map($cities,'id','name');

        return $this->render('/smi/edit', [
            'model' => $model,
            'cities' => $cities,
        ]);
    }

    public function actionSave()
    {
        $model = new SmiEdit();
        if ($model->load($_POST) && $model->validate() && $model->save()) {
            Yii::$app->session->addFlash('success', 'Изменения сохранены');
        }

        return $this->redirect(Url::to(['/catalog-smi/list']));
    }
}