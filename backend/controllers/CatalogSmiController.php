<?php
namespace backend\controllers;


use backend\models\ar\ArPFSmi;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use backend\models\data_provider\CatalogSmiProviderBuilder;
use yii\helpers\Url;
use function GuzzleHttp\json_encode;

class CatalogSmiController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['be_catalog_smi'],
                    ],
                ],
            ]
        ]);
    }

    public function actionList()
    {
        $request = Yii::$app->request;

        $builder = new CatalogSmiProviderBuilder();
        $builder->load($request->queryParams);

        Url::remember();

        if ($builder->validate()) {
            $dataProvider = $builder
                ->build()
                ->getDataProvider();

            return $this->render('/catalog_smi/list', [
                'searchModel' => $builder,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            $session = Yii::$app->getSession();
            $session->setFlash('error', $builder->getErrors());
            return '';
        }
    }

    public function actionRemove()
    {
        $request = Yii::$app->request;
        $pfsmi_id = (int)$request->get('id');

        if (ArPFSmi::removeFromCatalog($pfsmi_id)) {
            Yii::$app->session->addFlash('success', 'СМИ удален из каталога');
        }

        $this->redirect(Url::previous());
    }

    public function actionAdd()
    {
        $request = Yii::$app->request;
        $pfsmi_id = (int)$request->get('id');

        if (ArPFSmi::addToCatalog($pfsmi_id)) {
            Yii::$app->session->addFlash('success', 'СМИ добавлен в каталог');
        }

        $this->redirect(Url::previous());
    }

    public function actionClose()
    {
        $request = Yii::$app->request;
        $pfsmi_id = (int)$request->get('id');

        if (ArPFSmi::close($pfsmi_id)) {
            Yii::$app->session->addFlash('success', 'СМИ закрыто');
        }

        $this->redirect(Url::previous());
    }

    public function actionOpen()
    {
        $request = Yii::$app->request;
        $pfsmi_id = (int)$request->get('id');

        if (ArPFSmi::open($pfsmi_id)) {
            Yii::$app->session->addFlash('success', 'СМИ открыто');
        }

        $this->redirect(Url::previous());
    }

    public function actionApproved()
    {
        $request = Yii::$app->request;
        $pfsmi_id = (int)$request->get('id'); 
        $status = (int)$request->get('status');     
        if (ArPFSmi::approved($pfsmi_id, $status)) {
            return json_encode(['message' => 'Статус успешно изменен']);
        }
    }
}