<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `{{%Smi}}`.
 */
class m190212_203634_add_created_at_column_to_Smi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('Smi', 'created_at', $this->dateTime()->notNull()->defaultValue(new \yii\db\Expression('NOW()')));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('Smi', 'created_at');
    }
}
