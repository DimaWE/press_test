<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `{{%Smi}}`.
 */
class m190212_203711_add_updated_at_column_to_Smi_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('Smi', 'updated_at', $this->dateTime()->notNull()->defaultValue(new \yii\db\Expression('NOW()')));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('Smi', 'updated_at');
    }
}
